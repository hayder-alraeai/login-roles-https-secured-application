package com.hayder.loginsecured.controller;

import com.hayder.loginsecured.repository.StudentDao;
import com.hayder.loginsecured.student.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/students")
public class StudentController {
    private final StudentDao repo;
    @Autowired
    public StudentController(StudentDao repo) {
        this.repo = repo;
    }
    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    List<Student> all(){
        return repo.all();
    }
    @GetMapping("{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    Student one(@PathVariable int id){
        return repo.one(id);
    }
    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    void add(@RequestBody Student student){
        repo.add(student);
    }
    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    void delete(@PathVariable int id){
        repo.delete(id);
    }
    @PutMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    void update(@PathVariable int id, @RequestBody Student newStudent){
        repo.update(id, newStudent);
    }
}
