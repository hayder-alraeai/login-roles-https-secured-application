package com.hayder.loginsecured.repository;

import com.hayder.loginsecured.student.Student;

import java.util.List;

public interface StudentDao {
    List<Student> all();
    Student one(int id);
    void add(Student student);
    void delete(int id);
    void update(int id, Student newStudent);
}
