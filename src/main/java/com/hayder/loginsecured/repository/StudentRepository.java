package com.hayder.loginsecured.repository;

import com.hayder.loginsecured.student.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository implements StudentDao {
    private final static List<Student> studentList = new ArrayList<>();

    public StudentRepository() {
        studentList.add(new Student(1,"Johan", 30));
        studentList.add(new Student(2, "Rebecca", 29));
        studentList.add(new Student(3, "Micke" , 27));
    }

    @Override
    public List<Student> all() {
        return studentList;
    }

    @Override
    public Student one(int id) {
        return studentList.stream()
                .filter(p -> p.getId() == id)
                .findFirst()
                .orElseThrow();
    }

    @Override
    public void add(Student student) {
        studentList.add(student);
    }

    @Override
    public void delete(int id) {
        studentList.remove(one(id));
    }

    @Override
    public void update(int id, Student newStudent) {
        if (studentList.contains(one(id))){
            one(id).setName(newStudent.getName());
            one(id).setGrade(newStudent.getGrade());
        }else {
            newStudent.setId(id);
            studentList.add(newStudent);
        }
    }
}
